# example cascrel experiment

## About

This is an example experiment that compares the performance
of the Cascade Correlation Architecture and common machine learning methods.

## Usage

The recommended way to browse the notebook is to use pipenv to preinstall
dependencies into a new virtual env.

### Install pipenv

Obviously you should have `pip` already installed.

```
pip install --user pipenv
```

### Start the pipenv shell

```
pipenv shell
```

### Install `tensorflow` and `pycascrel` within the virtual env

Unfortunately installing `tensorflow` and `pycascrel` with pipenv
is currently very problematic so just run these within the pipenv shell:

```
pip install tensorflow pycascrel
```

### Run jupyter

```
jupyter lab
```

You can also just run the good old notebook:

```
jupyter notebook
```
